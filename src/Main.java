import checker.GPXPoint;
import checker.Result;
import checker.TrackHandler;
import html.HTML_Creator;

import org.xml.sax.SAXException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

public class Main {

    public static final String GPX_DATA_DIR = "./gpx_data/";
    public static final String HTML_DATA_DIR = "./html_data/";
    //private static final String ANALYSE_FILE_NAME = "./gpx_data/output.html";
    public static final int[] averageSize = {1, 2, 4, 8, 16, 32};
    public static final double[] minimumStepSize = {0, 1, 2, 3, 4, 5, 6, 8, 9, 10, 12, 16, 32, 64, 100};

    public static void main(String[] args) {
        TrackHandler trackHandler = null;
        SAXParser saxParser = null;
        try {
            saxParser = SAXParserFactory.newInstance().newSAXParser();
            trackHandler = new TrackHandler();
        } catch (ParserConfigurationException | SAXException e) {
            e.printStackTrace();
        }
        for (File fileEntry : Objects.requireNonNull(new File(GPX_DATA_DIR).listFiles())) {
            if (fileEntry.isFile() && fileEntry.toString().contains(".gpx")) {
                System.out.println("Prozessing: " + fileEntry.getName());
                try {
                    assert saxParser != null;
                    saxParser.parse(fileEntry, trackHandler);
                } catch (IOException e) {
                    e.printStackTrace();
                    System.out.println("File " + fileEntry + " NOT found!");
                } catch (SAXException e) {
                    e.printStackTrace();
                }
                HTML_Creator.writeToHTML(trackHandler.getTrack(),
                        trackHandler.getTourName(),
                        trackHandler.getAuthor(),
                        trackHandler.getUpAndDownhillInfo(),
                        averageSize,
                        minimumStepSize,
                        HTML_DATA_DIR
                        );
            }
        }

    }

    private static Result simple_sum(ArrayList<GPXPoint> track) {
        Result result = new Result();
        for (int i = 1; i < track.size(); i++) {
            double h_diff = 0;
            h_diff = track.get(i).height - track.get(i - 1).height;
            if (h_diff >= 0) result.uphill += h_diff;
            else result.downhill += h_diff;
        }
        return result;
    }
}
