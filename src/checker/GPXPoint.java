package checker;

public class GPXPoint {
    public double latitude;
    public double longitude;
    public double height;

    public GPXPoint(double latitude, double longitude, double height) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.height = height;
    }

    @Override
    public String toString() {
        return "GPXpoint {" +
                "latitude = " + latitude +
                ", longitude = " + longitude +
                ", height = " + height +
                '}';
    }
}
