package html;

import checker.GPXPoint;
import checker.Result;
import checker.ResultMatrix;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class HTML_Creator {

    static final String FS = "%.1f";

    public static void writeToHTML(ArrayList<GPXPoint> track,
                                   String tourName,
                                   String author,
                                   String upAndDownhillInfo,
                                   int[] averageSize,
                                   double[] minimumStepSize,
                                   String html_data_dir) {
        //FileWriter outFile = new FileWriter();
        StringBuilder contend = new StringBuilder();
        // html -> <table>
        contend.append("<!DOCTYPE HTML>\n" +
                "<html lang=\"de\">\n" +
                "\t<head>\n" +
                "\t\t<meta charset=\"UTF-8\">\n" +
                "\t\t<title>\n" +
                "\t\t\tAnalyse GPX Datei " + tourName + "\n" +
                "\t\t</title>\n" +
                "\t\t<style>\n" +
                "\t\t\ttable, td, th {\n" +
                "\t\t\t\tborder: 1px solid black;\n" +
                "\t\t\t}\n" +
                "\t\t\ttable {\n" +
                "\t\t\t\tborder-collapse: collapse;\n" +
                "\t\t\t}\n" +
                "\t\t\ttd {\n" +
                "\t\t\t\ttext-align: right;\n" +
                "\t\t\t\tpadding: 15px;\n" +
                "\t\t\t}\n" +
                "\t\t</style>\n" +
                "\t</head>\n" +
                "\t<body>\n" +
                "\t\t<p>\n" +
                "\t\t\tTour: " + tourName + "\n" +
                "\t\t</p>\n" +
                "\t\t\tAutor: " + author + "\n" +
                "\t\t<p>\n" +
                "\t\t</p>\n" +
                "\t\t<table>\n");
        // html TABLE contend
        // html table header
        contend.append("\t\t\t<tr>\n");
        contend.append("\t\t\t\t<th>Änderundsminimum</th>\n");

        for (double mSS : minimumStepSize) {
            contend.append("\t\t\t\t<th>" +
                    mSS +
                    "</th>\n");
        }
        contend.append("\t\t\t</tr>\n");
        contend.append("\t\t\t<tr>\n");
        contend.append("\t\t\t\t<th>" +
                "Mittelwert über<br>Elemente:" +
                "\t\t\t\t</th>\n");
        contend.append("\t\t\t</tr>\n");
        // table contend
        ResultMatrix resultMatrix = new ResultMatrix(track, averageSize, minimumStepSize, upAndDownhillInfo);
        for (int row = 0; row < averageSize.length; row++) {
            contend.append("\t\t\t<tr>\n" +
                    "\t\t\t\t<th>" +
                    String.format("%.0f", resultMatrix.getCollapsedTrackSize(row)) +
                    " x " +
                    averageSize[row] +
                    "</th>\n");
            for (int col = 0; col < minimumStepSize.length; col++) {
                //contend.append("\t\t\t\t<td>" + sum_if_more_than_Xm(collapsedTrack, minimumStepSize[col]).toShortHTMLString() + "</td>\n");
                //contend.append("\t\t\t\t<td>" + resultMatrix.getResult(row, col).toShortHTMLString() + "</td>\n");
                contend.append(createTableEntryforResult(resultMatrix.getResult(row, col)));
                //System.out.println("\t\t\t\t<td>" + row + ":" + col + "</td>\n");
            }
            contend.append("\t\t\t</tr>\n");
        }
        // html </table> ->
        contend.append("\t\t</table>\n");
        contend.append("\t\t<p>\n");
        contend.append("\t\t\tOriginalwerte gemessen von der Uhr: " + upAndDownhillInfo + "\n");
        contend.append("\t\t</p>\n");
        contend.append("\t</body>\n" +
                "</html>");
        //System.out.println(contend);
        //String plusOrMinus = "+";
        //Pattern pattern = Pattern.compile("\\" + plusOrMinus + "[0-9]+");
        //Matcher matcher = pattern.matcher(upAndDownhillInfo);
        //if (matcher.find())
        //    System.out.println(matcher.group());
        try {
            System.out.println("Writing HTML-file: " + html_data_dir + tourName + ".html");
            BufferedWriter fileWriter = new BufferedWriter(new FileWriter(html_data_dir + tourName + ".html"));
            fileWriter.write(contend.toString());
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String createTableEntryforResult2(Result result) {
        StringBuilder s = new StringBuilder();
        if (result.status < 100)
            s = s.append("\t\t\t\t<td>");
        else {
            s = s.append("\t\t\t\t<td style=\"background:red\">");
            result.status -= 100;
        }
        switch (result.status) {
            case 0:
                s.append("+" + String.format(FS, result.uphill) + "<br>" + String.format(FS, result.downhill));
                break;
            case 1:
                s.append("<div style=\"color:blue\">+" + String.format(FS, result.uphill) + "</div>" + String.format(FS, result.downhill));
                break;
            case 10:
                s.append("+" + String.format(FS, result.uphill) + "<br><div style=\"color:blue\">" + String.format(FS, result.downhill) + "</div>");
                break;
            case 11:
                s.append("<div style=\"color:blue\">+" + String.format(FS, result.uphill) + "</div><div style=\"color:blue\">" + String.format(FS, result.downhill) + "</div></td>\n");
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + result.status);
        }
        s.append("</td>\n");
        return s.toString();
    }

    public static String createTableEntryforResult(Result result) {
        String tdStartTag = new String("\t\t\t\t<td>");
        String divStartTagUphill = new String("<div>");
        String divStartTagDownhill = new String("<div>");
        if (result.status >= ResultMatrix.IS_BEST_DIFFERENCE) {
            tdStartTag = new String("\t\t\t\t<td style=\"background:yellow\">");
            result.status -= ResultMatrix.IS_BEST_DIFFERENCE;
        }
        if (result.status >= ResultMatrix.IS_BEST_OVERALL) {
            tdStartTag = new String("\t\t\t\t<td style=\"background:red\">");
            result.status -= ResultMatrix.IS_BEST_OVERALL;
        }
        if (result.status >= ResultMatrix.IS_BEST_DOWNHILL) {
            divStartTagDownhill = new String("<div style=\"color:blue\">");
            result.status -= ResultMatrix.IS_BEST_DOWNHILL;
        }
        if (result.status >= ResultMatrix.IS_BEST_UPHILL) {
            divStartTagUphill = new String("<div style=\"color:blue\">");
            result.status -= ResultMatrix.IS_BEST_UPHILL;
        }
        return new String(
                tdStartTag +
                        divStartTagUphill +
                        String.format(FS, result.uphill) +
                        "</div><br>" +
                        divStartTagDownhill +
                        String.format(FS, result.downhill) +
                        "</div>" +
                        "</td> +" +
                        "\n");
    }
}
