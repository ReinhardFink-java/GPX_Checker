package tests;

import checker.GPXPoint;
import checker.ResultMatrix;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class ResultMatrixTest {

    ArrayList<GPXPoint> track;
    ResultMatrix result;

    @Test
    public void testTrackWithZeros() {
        int[] averageSize = {1};
        double[] minimumStepSize = {0};
        String upAndDownhillInfo = "0";
        track = new ArrayList<>();

        for (int i = 0; i < 10; i++)
            track.add(new GPXPoint(0, 0, 0));

        result = new ResultMatrix(track, averageSize, minimumStepSize, upAndDownhillInfo);
        assertEquals(0.0, result.getResult(0, 0).uphill);
        assertEquals(0.0, result.getResult(0, 0).downhill);
    }

    @Test
    public void testTrackWithOneElementUp() {
        int[] averageSize = {1};
        double[] minimumStepSize = {0};
        String upAndDownhillInfo = "0";
        track = new ArrayList<>();

        for (int i = 0; i < 10; i++)
            track.add(new GPXPoint(0, 0, 0));
        track.set(5, new GPXPoint(0, 0, 1));

        result = new ResultMatrix(track, averageSize, minimumStepSize, upAndDownhillInfo);
        assertEquals(1.0, result.getResult(0, 0).uphill);
        assertEquals(-1.0, result.getResult(0, 0).downhill);
    }

    @Test
    public void testTrackWithOneElementUpAndMinimumStepSizeOne() {
        int[] averageSize = {1};
        double[] minimumStepSize = {1};
        String upAndDownhillInfo = "0";
        track = new ArrayList<>();

        for (int i = 0; i < 10; i++)
            track.add(new GPXPoint(0, 0, 0));
        track.set(5, new GPXPoint(0, 0, 1));

        result = new ResultMatrix(track, averageSize, minimumStepSize, upAndDownhillInfo);
        assertEquals(0.0, result.getResult(0, 0).uphill);
        assertEquals(0.0, result.getResult(0, 0).downhill);

        track.set(5, new GPXPoint(0, 0, 1.1));

        result = new ResultMatrix(track, averageSize, minimumStepSize, upAndDownhillInfo);
        assertEquals(1.1, result.getResult(0, 0).uphill);
        assertEquals(-1.1, result.getResult(0, 0).downhill);
    }

    @Test
    public void testTrackWithTwoElementUpAndMinimumStepSizeOne() {
        int[] averageSize = {1};
        double[] minimumStepSize = {1};
        String upAndDownhillInfo = "0";
        track = new ArrayList<>();

        for (int i = 0; i < 10; i++)
            track.add(new GPXPoint(0, 0, 0));
        track.set(7, new GPXPoint(0, 0, 1.1));
        track.set(8, new GPXPoint(0, 0, 1.0));
        track.set(9, new GPXPoint(0, 0, 1.0));

        result = new ResultMatrix(track, averageSize, minimumStepSize, upAndDownhillInfo);
        assertEquals(1.1, result.getResult(0, 0).uphill);
        assertEquals(0.0, result.getResult(0, 0).downhill);

        track.set(9, new GPXPoint(0, 0, 0.0));

        result = new ResultMatrix(track, averageSize, minimumStepSize, upAndDownhillInfo);
        assertEquals(1.1, result.getResult(0, 0).uphill);
        assertEquals(-1.1, result.getResult(0, 0).downhill);
    }

    @Test
    public void testTrackWithUpAndDowns() {
        int[] averageSize = {1};
        double[] minimumStepSize = {1};
        String upAndDownhillInfo = "0";
        track = new ArrayList<>();
        for (int i = 0; i < 10; i++)
            track.add(new GPXPoint(0, 0, 0));
        track.set(0, new GPXPoint(0, 0, 1.1));
        track.set(1, new GPXPoint(0, 0, 1.0)); //-0.1
        track.set(2, new GPXPoint(0, 0, 1.2)); //+0.2
        track.set(3, new GPXPoint(0, 0, 1.0)); //-0.2
        track.set(4, new GPXPoint(0, 0, 1.3)); //+0.3
        track.set(5, new GPXPoint(0, 0, 1.0)); //-0.3
        track.set(6, new GPXPoint(0, 0, 1.1)); //+0.1
        track.set(7, new GPXPoint(0, 0, 0.1)); //-1.0
        track.set(8, new GPXPoint(0, 0, 1.0)); //+0.9
        track.set(9, new GPXPoint(0, 0, 1.8)); //+0.8
        // +2.3 //-1.6

        result = new ResultMatrix(track, averageSize, minimumStepSize, upAndDownhillInfo);
        assertEquals(0.0, result.getResult(0, 0).uphill);
        assertEquals(0.0, result.getResult(0, 0).downhill);

        minimumStepSize[0] = 0;
        result = new ResultMatrix(track, averageSize, minimumStepSize, upAndDownhillInfo);
        assertEquals(2.3, result.getResult(0, 0).uphill);
        assertEquals(-1.6, result.getResult(0, 0).downhill);

        minimumStepSize[0] = 0.5;
        result = new ResultMatrix(track, averageSize, minimumStepSize, upAndDownhillInfo);
        assertEquals(1.7, result.getResult(0, 0).uphill, 1E-10);
        assertEquals(-1.0, result.getResult(0, 0).downhill);
    }
}