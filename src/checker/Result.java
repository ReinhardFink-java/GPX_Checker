package checker;

public class Result {

    public double uphill;
    public double downhill;
    public int status;
    // 1 for best uphill
    // 10 for best downhill
    // 100 for best overall
    // and all 8 mixtures

    @Override
    public String toString() {
        return "Result {" +
                "uphill = " + uphill +
                ", downhill =  " + downhill +
                '}';
    }
}
