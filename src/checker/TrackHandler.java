package checker;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;

/**
 * we parse GPX -file looking like:
 * <?xml version="1.0" encoding="UTF-8" standalone="no"?>
 * <gpx xmlns="http://www.topografix.com/GPX/1/1"
 * xmlns:gpxtpx="http://www.garmin.com/xmlschemas/TrackPointExtension/v1"
 * xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" creator="Suunto app" version="1.1"
 * xsi:schemaLocation="http://www.topografix.com/GPX/1/1
 * http://www.topografix.com/GPX/1/1/gpx.xsd
 * http://www.garmin.com/xmlschemas/TrackPointExtension/v1
 * http://www.garmin.com/xmlschemas/TrackPointExtensionv1.xsd">
 * <metadata>
 * 	<name>Blindseetrail Reini</name>
 * 	<author><name>Reini Fink</name></author>
 * 	<desc>Uphill: +1099; Downhill: -1101 Höhenlinien gezählt:</desc>
 * </metadata>
 * <rte>
 * <rtept lat="47.357483" lon="10.82574"/>
 * <rtept lat="47.357483" lon="10.82574"/>
 * <rtept lat="47.357452" lon="10.82579"><ele>1181.8</ele></rtept>
 *
 * OR
 *
 * <metadata>
 *     <name>Pians_Strengen_Runde</name>
 *     <author>
 *       <link href="https://www.komoot.de">
 *         <text>komoot</text>
 *         <type>text/html</type>
 *       </link>
 *     </author>
 *   </metadata>
 *   <trk>
 *     <name>Pians_Strengen_Runde</name>
 *     <trkseg>
 *       <trkpt lat="47.135096" lon="10.514332">
 *         <ele>866.606931</ele>
 *         <time>2022-08-02T09:13:29.307Z</time>
 *       </trkpt>
 *       <trkpt lat="47.135181" lon="10.514542">
 *         <ele>866.606931</ele>
 *         <time>2022-08-02T09:13:36.122Z</time>
 *       </trkpt>
 */

public class TrackHandler extends DefaultHandler {

    // tour info from GPX file
    private String tourName;
    private String author;
    // Up & Down Info, manually added to GPX file
    private String upAndDownhillInfo;
    // the tracker points
    private ArrayList<GPXPoint> track;

    private String elementValue;

    private boolean isInsideMetadata;
    private boolean isInsideAuthor;

    private GPXPoint gpxPoint;

    public TrackHandler() {
        this.tourName = "";
        this.author = "";
        this.upAndDownhillInfo = "";
        this.track = new ArrayList<GPXPoint>();
        this.isInsideMetadata = false;
        this.isInsideAuthor = false;
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        elementValue = new String(ch, start, length);
    }

    @Override
    public void startDocument() throws SAXException {
        track = new ArrayList<GPXPoint>();
        isInsideMetadata = false;
        isInsideAuthor = false;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attr) throws SAXException {
        //System.out.println("uri: " + uri + " localName: " + localName + " qName: " + qName + " attr: " + attr);
        switch (qName) {
            case "rtept":
                gpxPoint = new GPXPoint(Double.parseDouble(attr.getValue(0)), Double.parseDouble(attr.getValue(1)),0);
                break;
            case "trkpt":
                gpxPoint = new GPXPoint(Double.parseDouble(attr.getValue(0)), Double.parseDouble(attr.getValue(1)),0);
                break;
            case "author":
                isInsideAuthor = true;
                break;
            case "metadata":
                isInsideMetadata = true;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        switch (qName) {
            case "ele":
                // avoid to add elements with missing height
                if (!elementValue.isEmpty()) {
                    //System.out.println("elem has value " + elementValue);
                    gpxPoint.height = Double.parseDouble(elementValue);
                    track.add(gpxPoint);
                    //System.out.println("elem has value " + gpxPoint);
                }
                break;
            case "desc":
                if (elementValue.contains("Uphill"))
                    upAndDownhillInfo = elementValue;
                break;
            case "metadata":
                isInsideMetadata = false;
                break;
            case "author":
                isInsideAuthor = false;
                break;
            case "name":
                if (isInsideMetadata) {
                    if (isInsideAuthor)
                        author = elementValue;
                    else
                        tourName = elementValue;
                }
                break;
        }
    }

    public ArrayList<GPXPoint> getTrack() {
        return track;
    }

    public String getUpAndDownhillInfo() {
        return upAndDownhillInfo;
    }

    public String getTourName() {
        return tourName;
    }

    public String getAuthor() {
        return author;
    }
}