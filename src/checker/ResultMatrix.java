package checker;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ResultMatrix {

    public static final int IS_BEST_UPHILL = +1;
    public static final int IS_BEST_DOWNHILL = +10;
    public static final int IS_BEST_OVERALL = +100;
    public static final int IS_BEST_DIFFERENCE = +1000;

    private final ArrayList<GPXPoint> track;
    private final int[] averageSize;
    private final double[] minimumStepSize;
    private final String upAndDownhillInfo;
    private final Result[][] matrix;
    // we need size of the collapsed tracks for output
    private final int[] collapsedTrackSize;

    public ResultMatrix(ArrayList<GPXPoint> track, int[] averageSize, double[] minimumStepSize, String upAndDownhillInfo) {
        this.track = track;
        this.averageSize = averageSize;
        this.minimumStepSize = minimumStepSize;
        this.upAndDownhillInfo = upAndDownhillInfo;
        collapsedTrackSize = new int[averageSize.length];
        matrix = fillResultMatrix();
    }

    public Result getResult(int row, int col) {
        return matrix[row][col];
    }

    public double getCollapsedTrackSize(int row) {
        return collapsedTrackSize[row];
    }

    private Result[][] fillResultMatrix() {
        Result[][] matrix = new Result[averageSize.length][minimumStepSize.length];
        for (int row = 0; row < averageSize.length; row++) {
            ArrayList<GPXPoint> collapsedTrack = collapse(track, averageSize[row]);
            collapsedTrackSize[row] = collapsedTrack.size();
            for (int col = 0; col < minimumStepSize.length; col++) {
                matrix[row][col] = sum_if_more_than_min_diff(collapsedTrack, minimumStepSize[col]);
            }
        }
        if (!upAndDownhillInfo.isEmpty())
            markBest(matrix, upAndDownhillInfo);
        return matrix;
    }

    /**
     *
     * where every GPXPoint is created as average of first and subListSize - 1 next points
     */
    private ArrayList<GPXPoint> collapse(ArrayList<GPXPoint> track, int subListSize) {
        ArrayList<GPXPoint> collapsedTrack = new ArrayList<>(track.size() / subListSize + subListSize);
        for (int i = 0; i < track.size(); i += subListSize)
            collapsedTrack.add(averageFromSlice(track, i, i + subListSize));
        return collapsedTrack;
    }

    /**
     * where we only count height differences, if they are bigger than min_diff
     */
    private Result sum_if_more_than_min_diff(ArrayList<GPXPoint> track, double min_diff) {
        Result result = new Result();
        GPXPoint first = track.remove(0);
        for (GPXPoint second : track) {
            double h_diff = second.height - first.height;
            if (h_diff > min_diff) {
                result.uphill += h_diff;
                first = second;
            } else if (h_diff < -min_diff) {
                result.downhill += h_diff;
                first = second;
            }
        }
        return result;
    }

    private GPXPoint averageFromSlice(ArrayList<GPXPoint> track, int left, int right) {
        double average_h = 0;
        int count = 0;
        GPXPoint p = track.get(left);
        for (int i = left; i < right; i++) {
            if (i < track.size()) {
                average_h += track.get(i).height;
                count++;
            }
        }
        // check for just one point, possible when slice hits end
        if (count != 0)
            p = new GPXPoint(p.latitude,p.longitude,average_h / count);
        return p;
    }

    private void markBest(Result[][] matrix, String upAndDownhillInfo) {
        Result bestUphill = matrix[0][0];
        Result bestDownhill = matrix[0][0];
        Result bestOverall = matrix[0][0];
        Result bestDifferenze = matrix[0][0];
        double uphill = getUpOrDownhill(upAndDownhillInfo, "+");
        double downhill = getUpOrDownhill(upAndDownhillInfo, "-");
        for (int row = 0; row < averageSize.length; row++) {
            for (int col = 0; col < minimumStepSize.length; col++) {
                if (Math.abs(matrix[row][col].uphill - uphill) < Math.abs(bestUphill.uphill - uphill))
                    bestUphill = matrix[row][col];
                if (Math.abs(matrix[row][col].downhill - downhill) < Math.abs(bestDownhill.downhill - downhill))
                    bestDownhill = matrix[row][col];
                if (Math.abs(matrix[row][col].uphill - uphill) + Math.abs(matrix[row][col].downhill - downhill)
                        < Math.abs(bestOverall.uphill - uphill) + Math.abs(bestOverall.downhill - downhill))
                    bestOverall = matrix[row][col];
                if (Math.abs(matrix[row][col].uphill + matrix[row][col].downhill) < Math.abs(bestDifferenze.uphill + bestDifferenze.downhill)) {
                    /*
                    System.out.print(matrix[row][col].uphill);
                    System.out.print(":");
                    System.out.print(matrix[row][col].downhill);
                    System.out.print(":");
                    System.out.println(Math.abs(matrix[row][col].uphill + matrix[row][col].downhill));

                     */
                    bestDifferenze = matrix[row][col];
                }
            }
        }
        bestUphill.status += IS_BEST_UPHILL;
        bestDownhill.status += IS_BEST_DOWNHILL;
        bestOverall.status += IS_BEST_OVERALL;
        bestDifferenze.status += IS_BEST_DIFFERENCE;
    }


    private double getUpOrDownhill(String upAndDownhillInfo, String plusOrMinus) {
        StringBuilder patternText = new StringBuilder();
        patternText.append("\\");
        patternText.append(plusOrMinus);
        patternText.append("[0-9]+");
        Pattern pattern = Pattern.compile(patternText.toString());
        Matcher matcher = pattern.matcher(upAndDownhillInfo);
        if (matcher.find())
            return Double.parseDouble(matcher.group());
        else
            return 0;
    }
}

